<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Career</title>
<link rel="stylesheet" href="css/bootstrap.min.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="fontawesome/css/all.min.css">
</head>
<body>

<?php include("nav.php");?>

<!--Banner-->
<div class="banner-career">
<div class="container-fluid">
<h2 class="d-flex justify-content-center" style="color:white;">Career</h2> 
</div>
</div>



<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<div class="container">
<div class="collapse" id="collapseExample">

<h5>Responsibilities</h5>

<ul>
<li>Translate concepts into user flows,wireframes,mockups and prototypes that lead to intuitive user experiences.</li>

<li>Facilitate the client’s product vision by researching, conceiving, sketching, prototyping and user-testing experiences for digital products.</li>

<li>Design and deliver wireframes, user stories, user journeys, and mockups optimized for a wide range of devices and interfaces.</li>

<li>Identify design problems and devise elegant solutions.</li>

<li>Make strategic design and user-experience decisions related to core, and new functions and features.</li>

<li>Take a user-centered design approach and rapidly test and iterate your designs.</li>

<li>Collaborate with other team members and stakeholders.</li>

<li>Ask smart questions, take risks and champion new ideas.</li>
</ul>

<h5>Requirements</h5>

<ul >
  <li>Expertise in standard UX software such as Sketch, OmniGraffle, Axure, InVision, UXPin, Balsamiq, Framer, and the like is a must. Basic HTML5, CSS3, and JavaScript skills are a plus. Extensive experience in using UX design best practices to design solutions, and a deep understanding of mobile-first and responsive design.</li>
  <li> A solid grasp of user-centered design (UCD), planning and conducting user research, user testing, A/B testing, rapid prototyping, heuristic analysis, usability and accessibility concerns. Ability to iterate designs and solutions efficiently and intelligently.</li>
  <li>Ability to clearly and effectively communicate design processes, ideas, and solutions to teams and clients. </li>
  <li>A clear understanding of the importance of user-centered design and design thinking.</li>
  <li>Ability to work effectively in a team setting including synthesizing abstract ideas into concrete design implications.</li>
  <li>Be excited about collaborating and communicating closely with teams and other stakeholders via a distributed model, to regularly deliver design solutions for approval. Be passionate about resolving user pain points through great design.</li>
  <li> Be open to receiving feedback and constructive criticism.</li>
  <li>Be passionate about all things UX and other areas of design and innovation. Research and showcase knowledge in the industry’s latest trends and technologies.</li>
</ul>
</div>
</div>


<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>

<nav class="navbar navbar-expand-lg navbar-light" style="margin-left:150px; margin-right:220px;">
<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample08" aria-controls="navbarsExample08" aria-expanded="false" aria-label="Toggle navigation">
<span class="navbar-toggler-icon"></span>
</button>

<div class="collapse navbar-collapse navbar-custom justify-content-md-center" id="navbarsExample08">
<ul class="navbar-nav">
<li class="nav-item active">
<a class="nav-link" href="#">UI/UX(Negotiable) <span class="sr-only">(current)</span></a>
</li>
<li class="nav-item">
<a class="nav-link" href="#">Full Time</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#">Posted 19 October 2020</a>
</li>
<li class="nav-item">
<a class="nav-link " href="#"> <i class="fas fa-chevron-down"></i></a>
</li>

</ul>
</div>
</nav>









<!--Footer-->
<footer class="footer-area mt-5">
<div class="container-fluid">
<div class="row text-center">
<div class="col-md-4 sm-12 p-4 bg-ingfo">
<i class="far fa-envelope"></i>
Email <br>
info@mml.com.mm
</div>

<div class="col-md-4 sm-12 p-4 bg-time">
<i class="far fa-clock"></i>
Working hours <br>
Mon-Fri 9AM - 6PM
</div>

<div class="col-md-4  sm-12 p-4 bg-contact">
<i class="fas fa-phone-alt"></i>
Telephone <br>
+959 9759 09701 <br>
+959 4411 95962
</div>
</div>
</div>


<section class="info">
<div class="container my-5">
<div class="row">
<div class="col-md-4">
<h6>HEAD OFFICE</h6>
<p>MAPCO Building.No(100).3rd Floor,Wardan Street& Kan Nar
Street Beside the concrete Express way Wardan Port Area
Lanmadaw Township Yangon
</p><br>


<h6>OFFICE ADDRESS</h6>
<p>MAPCO Building.No(100).3rd Floor,Wardan Street& Kan Nar
Street Beside the concrete Express way Wardan Port Area
Lanmadaw Township Yangon</p>

</div>

<div class="col-md-4 ">
<h6>USEFUL LINKS</h6>

<div class="row">
<div class="col-md-6">
<ul class="list-unstyled">
<li><a href="index.php">Home</a></li>
  <li><a href="services.php">Services</a></li>
  <li><a href="news.php">News</a></li>
  <li><a href="career.php">Career</a></li>
  <li><a href="about-us.php">About us</a></li>
</ul>

</div>
<div class="col-md-6">
<ul class="list-unstyled">
<li>Carrer</li>
<li>Reviews</li>
<li>Terms & Conditions</li>
<li>Help</li>
<li>Events</li>
</ul>
</div>
</div>


</div>


<div class="col-md-4">
<h6>Events</h6>


<div class="container my-3">
<div class="row">
<div class="col-md-6">
<img src="images/mml1.jpg" class="img-fluid"alt="">
</div>
<div class="col-md-6">
<img src="images/mml2.jpg" class="img-fluid"alt="">
</div>
</div>

<div class="row">
<div class="col-md-6">
<img src="images/mml3.jpg" class="img-fluid"alt="">
</div>
<div class="col-md-6">
<img src="images/mml4.jpg" class="img-fluid"alt="">
</div>
</div>
</div>
</div>



</div>
</div>
</div>
</section>

<div class="copyright-wrap py-3">
<div class="container">
<div class="row">
<div class="col-12">
<div class="copyright-text text-center">

<p>© Copyright 2019 Myanmar Media Linkage. All rights reserved.</p>

</div>
</div>
</div>
</div>
</div>

</div>


</footer>


<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
crossorigin="anonymous"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/jquery-min.js"></script>
</body>
</html>